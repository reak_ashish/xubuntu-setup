# Fantastic Setup and How to do it !

[![REAK](http://reak.in/reak_logo_small.png)](https://reak.in)


## About

As a webdeveloper, this is just an attempt to jot down the customizations on my dev machine so it's easy to clone for my employees or anyone else who finds this productive.

Feel free to fork this, or add in something that I missed via PR.


## Setup

### Built on Xubuntu (Ubuntu + XFCE)

##### Install Zsh

* `sudo apt-get install zsh`

##### Install OhMyZsh

* `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
* Accept prompt to make zsh default
* Help : https://github.com/robbyrussell/oh-my-zsh

##### Customizing OhMyZsh
* Powerfonts
    * Install powerfonts - `sudo apt-get install fonts-powerline`
    * Powerfonts Help - https://github.com/powerline/fonts
* Agnoster
    * Install Agnoster
        * Open `~/.zshrc`
        * Change `ZSH_THEME=agnoster`
        * Voila
    * Agnoster Help : https://github.com/agnoster/agnoster-zsh-theme

##### Customizing Native Terminal
* Transperancy
    * Go into Preferences -> Appearance -> Background
    * Select Transparent Background
    * Adjust opacity
        
* Colors
    * Agnoster works awesome with solarized colors
    * To setup solarized colors,
    * `cd ~/.config/xfce4/`
    * `mkdir terminal`
    * `touch terminalrc`
    * `cd`
    * `git clone git@github.com:sgerrand/xfce4-terminal-colors-solarized.git`
    * `cd xfce4-terminal-colors-solarized`
    * `cp dark/terminalrc ~/.config/xfce4/terminal/terminalrc`
    * Solarized Help - https://github.com/sgerrand/xfce4-terminal-colors-solarized


##### Customizing Look of XFCE
* Theme
    * Arc Theme
    * `git clone git@github.com:horst3180/arc-theme.git`
    * `sudo apt-get install build-essential autoconf automake pkg-config libgtk-3-dev`
    * `./autogen.sh --prefix=/usr`
    * `sudo make install`
    * Theme should be available in All Settings -> Appearance -> Arc-Dark
    * All Settings -> Window Manager -> Arc-Dark
    * `xfconf-query -c xfce4-desktop -p /desktop-icons/center-text -n -t bool -s false // Removes shadow bug`
    * Arc Help - https://github.com/horst3180/arc-theme

* Icons
    * `sudo add-apt-repository ppa:moka/daily`
    * `sudo apt-get update`
    * `sudo apt-get install moka-icon-theme faba-icon-theme faba-mono-icons`
    * Moka Help - https://github.com/snwh/moka-icon-theme


##### IDE / Editors Used & Config

* VSCode
    * Plugins List : https://gitlab.com/reak_ashish/xubuntu-setup/blob/master/VSCodeExtensions.md
