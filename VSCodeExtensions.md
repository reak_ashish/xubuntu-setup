## Visual Studio Plugins

#### My personal list of plugins that I use

```
Zignd.html-css-class-completion
alefragnani.project-manager
dbaeumer.vscode-eslint
donjayamanne.githistory
eamodio.gitlens
eg2.tslint
esbenp.prettier-vscode
fabiospampinato.vscode-todo-plus
felixfbecker.php-debug
felixfbecker.php-intellisense
formulahendry.auto-close-tag
formulahendry.code-runner
kokororin.vscode-phpfmt
ms-python.python
ms-vscode.cpptools
PeterJausovec.vscode-docker
robertohuertasm.vscode-icons
streetsidesoftware.code-spell-checker
tomoki1207.vscode-input-sequence
vincentkos.php-docblock-generator
vsmobile.cordova-tools
yzane.markdown-pdf
zhuangtongfa.Material-theme
```